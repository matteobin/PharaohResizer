PharaohResizer: PharaohResizer.c
	$(CC) -Os -ansi -pedantic $@.c -o $@ -llz4 

clean:
	rm -f PharaohResizer

.PHONY: clean
